<!DOCTYPE html>
<html lang="en">
<?php include('inc/sig.php');?>
  <head>
    <title>:: Asoka Homepage ::</title>
    <?php include('inc/load_top.php');?>
  </head>
  <body>

    <!-- Wrap all page content here -->
    <div id="wrap">

    <?php include('inc/header.php');?>
      
      
    <div class="content-bg">
   
    <div class="container">
     <div id="cat-heading">
       <span> Places </span>
     </div>
    </div>
    
    
     <div class="category-sort">
       <ul>
        <li><a href="#" class="">Places</a></li>
        <li><a href="#" class="">Yumms</a></li>
        <li><a href="#" class="">Save the world</a></li>
       </ul>
     </div>


    <div class="container">
      <div id="content" class="article">
        <ul class="article-list">
         <?php for($i=1; $i<9; $i++){ ?>
          <li class="item">
           <a href="#" class="thumb-article"><img class="img-responsive" src="uploads/article.png" alt="Generic placeholder image" ></a>
           <div class="caption">
            <span class="cat">Places</span>
            <h2>Heading</h2>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
            <p><a class="read-more" href="#">Read More &raquo;</a></p>
           </div>
          </li><!-- /.col-lg-3 col-md-3 col-sm-6 col-xs-12 -->
         <?php } ?>
         </ul>
      </div><!-- #content -->
     </div><!-- .container -->
     </div> <!-- .content-bg -->


    </div><!-- #wrap -->

    <?php include('inc/footer.php');?>

    <?php include('inc/load_bottom.php');?>

  </body>
</html>